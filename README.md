Project Members:
    -Towqir Ahmed Shaem ( towqirahmedshaem@gmail.com )
    -Momshad Dinury ( md.dinury@gmail.com )
    
Technology: ESP-NOW Portocol
Link : 
https://www.espressif.com/en/products/software/esp-now/overview
Length Coverage : 20-30 Meter

What is ESP-NOW?
ESP-Now is another protocol developed by Espressif, which enables multiple devices to communicate without using Wi-Fi. The protocol is similar to the low power 2.4GHz wireless connectivity that is often deployed in wireless mice. So, the pairing between devices is needed prior to the communication. After the pairing is done, the connection is persistent, peer-to-peer, and no handshake is required.
    
    As microcontroller I am using ESP8266.
